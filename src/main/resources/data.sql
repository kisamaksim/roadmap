insert into city (name, x, y) values ('A', 0, 0);
insert into city (name, x, y) values ('B', 50, 50);
insert into city (name, x, y) values ('C', 100, 100);
insert into city (name, x, y) values ('D', 1000, 1000);

insert into road (name, length, city_one, city_two) values ('M60', 25, 1, 2);
insert into road (name, length, city_one, city_two) values ('E95', 900, 3, 4);
insert into road (name, length, city_one, city_two) values ('E96', 100, 1, 3);