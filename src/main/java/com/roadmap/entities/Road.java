package com.roadmap.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity
public class Road {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;
    
    @Column(name = "NAME")
    private String name;
    
    @Column(name = "LENGTH")
    private Integer length;
    
    @ManyToOne
    @JoinColumn(name = "CITY_ONE")
    private City cityOne;
    
    @ManyToOne
    @JoinColumn(name = "CITY_TWO")
    private City cityTwo;
}
