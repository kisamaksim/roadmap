package com.roadmap.repositories;

import org.springframework.data.repository.CrudRepository;

import com.roadmap.entities.City;

public interface CityRepository extends CrudRepository<City, Integer> {
    
    City findByName(String name);
}
