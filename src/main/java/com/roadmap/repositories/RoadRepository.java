package com.roadmap.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.roadmap.entities.Road;

public interface RoadRepository extends JpaRepository<Road, Integer> {
    
    Road findByName(String name);
    List<Road> findByCityOne_NameOrCityTwo_Name(String cityOneName, String cityTwoName);
}
