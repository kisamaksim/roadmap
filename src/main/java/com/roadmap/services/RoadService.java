package com.roadmap.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.roadmap.entities.Road;
import com.roadmap.repositories.RoadRepository;

@RestController
@Transactional
public class RoadService {
    
    @Autowired
    private RoadRepository roadRepository;
    
    @PostMapping("/road/save")
    public Road save(@RequestBody Road road) {
        return roadRepository.save(road);
    }
    
    @DeleteMapping("/road/{name}")
    public void delete(@PathVariable String name) {
        Road byName = roadRepository.findByName(name);
        roadRepository.delete(byName);
    }
    
    @GetMapping("/road/{cityName}")
    public List<Road> getRoadsByCityName(@PathVariable String cityName) {
        return roadRepository.findByCityOne_NameOrCityTwo_Name(cityName, cityName);
    }
}
