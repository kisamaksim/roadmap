package com.roadmap.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.roadmap.entities.City;
import com.roadmap.repositories.CityRepository;

@RestController
@Transactional
public class CityService {
    
    @Autowired
    private CityRepository cityRepository;
    
    @GetMapping("/city/{name}")
    public City getCityByName(@PathVariable String name) {
        return cityRepository.findByName(name);
    }
    
    @PostMapping("/city/save")
    public City saveOrUpdate(@RequestBody City city) {
        return cityRepository.save(city);
    }
}
