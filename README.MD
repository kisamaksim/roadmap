# Как запустить

1. Склонировать проект
2. Запускать как Spring Boot приложение
3. Перейти по линке http://localhost:8080/swagger-ui.html 
    - city-service операции для городов
    - road-service операции для дорог